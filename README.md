#Metodologias implementadas 

En el desarrollo del back-end con Adonisjs, note que compartia algunas caracteristicas con el framework de php - Laravel, lo cual me parecio muy interesante, pero si tenia muchas variaciones las cuales me generaron disficultades durante el desarrollo, tuve muchos errores en cuanto a versionamientos, porque muchas veces la version de angular no me soportada la de Adonisjs, por lo cual tuve que comenzar el proyecto en 2 ocasiones de nuevo, pero a nivel general me parecio un buen framework para trabajar.

Al igual que en angular me base en video tutoriales de youtube y en lecturas de paginas web, trate de entender lo mejor que pude su funcionamiento.


# Adonis fullstack application

This is the fullstack boilerplate for AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Session
3. Authentication
4. Web security middleware
5. CORS
6. Edge template engine
7. Lucid ORM
8. Migrations and seeds

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```
