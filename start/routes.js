'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.post('/login', 'AuthController.login')

//creamos una ruta de tipo recurso para guardar nuestro usuario
Route.resource('/users','UserController')
    .apiOnly()//no permite generar rutas innecesarias
    //es necesario especificar el validador para cada ruta
    .validator(new Map([
        [['users.store'], ['storeUser']]
    ]))
