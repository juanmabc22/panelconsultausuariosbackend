'use strict'

const User = use('App/Models/User')

class UserController {

    async store({request, response}){
        //obtenemos los datos que me llegan desde el cliente
        const userData = request.only(['fullName','document','cellPhone','email', 'password', 'departament', 'city', 'neighborhood', 'address', 'salary', 'otherMoneyIncome', 'monthlyExpenses', 'financialExpenses'])
        const user = await User.create(userData)

        //si el usuario es creado correctamente
        return response.created({
            status: true,
            data: user
        })
    }
}

module.exports = UserController
