'use strict'

class AuthController {
    //ahora crearemos un metodo que nos permita generar nuestro toquen de login
    async login({request, response, auth}){
        const {email, password} = request.only(['email', 'password'])

        //crearemos nuestro token de usuario
        const token = await auth.attempt(email, password)

        return response.ok(token)
    }
}

module.exports = AuthController
