'use strict'

const {formatters} = use ('Validator')

class storeUser {
  get rules () {
    return {
      // validation rules
      fullName: 'required',
      document: 'required|number',
      cellPhone: 'required',
      email: 'required|email|max:240|unique:users,email',
      password: 'required|max:100',
      departament: 'required',
      city: 'required',
      neighborhood: 'required',
      address: 'required',
      salary: 'required|min:0',
      otherMoneyIncome:'required|min:0',
      monthlyExpenses: 'required|min:0',
      financialExpenses: 'required|min:0'
    }
  }

  get validateAll(){
    return true
  }

  get formatter(){
    return formatters.JsonApi
  }
}

module.exports = storeUser
