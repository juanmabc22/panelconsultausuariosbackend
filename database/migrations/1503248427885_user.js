'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('fullName', 254).notNullable()
      table.string('document', 10).notNullable()
      table.string('cellPhone', 10).notNullable()
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.string('departament', 50).notNullable()
      table.string('city', 50).notNullable()
      table.string('neighborhood', 50).notNullable()
      table.string('address', 100).notNullable()
      table.integer('salary').notNullable()
      table.integer('otherMoneyIncome').notNullable()
      table.integer('monthlyExpenses').notNullable()
      table.integer('financialExpenses').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema